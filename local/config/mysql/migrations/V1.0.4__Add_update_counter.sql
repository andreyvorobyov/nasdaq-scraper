CREATE TABLE nasdaqdb.update_counter (
    id INT NOT NULL AUTO_INCREMENT,
    last_update DATETIME NOT NULL,
    company_id INT,
    
    FOREIGN KEY (company_id)
    REFERENCES companies(id),
       
    PRIMARY KEY (id));
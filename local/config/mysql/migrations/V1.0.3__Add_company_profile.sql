CREATE TABLE nasdaqdb.companies_profile (
    id INT NOT NULL AUTO_INCREMENT,
    data_sourse VARCHAR(15) NOT NULL,
    company_id INT,
    
    FOREIGN KEY (company_id)
    REFERENCES companies(id),
       
    PRIMARY KEY (id));

CREATE TABLE nasdaqdb.companies_address (
    id INT NOT NULL AUTO_INCREMENT,
    address VARCHAR(200) NOT NULL,
    companyProfile_id INT,
    FOREIGN KEY (companyProfile_id)
    REFERENCES companies_profile(id),
 
    PRIMARY KEY (id));

CREATE TABLE nasdaqdb.companies_business_summary (
    id INT NOT NULL AUTO_INCREMENT,
    business_summary TEXT NOT NULL,
    companyProfile_id INT,
    FOREIGN KEY (companyProfile_id)
    REFERENCES companies_profile(id),
 
    PRIMARY KEY (id));

CREATE TABLE nasdaqdb.companies_fax (
    id INT NOT NULL AUTO_INCREMENT,
    fax VARCHAR(15) NOT NULL,
    companyProfile_id INT,
    FOREIGN KEY (companyProfile_id)
    REFERENCES companies_profile(id),
 
    PRIMARY KEY (id));

CREATE TABLE nasdaqdb.companies_phone (
    id INT NOT NULL AUTO_INCREMENT,
    phone VARCHAR(15) NOT NULL,
    companyProfile_id INT,
    FOREIGN KEY (companyProfile_id)
    REFERENCES companies_profile(id),
 
    PRIMARY KEY (id));


ALTER TABLE  nasdaqdb.companies_profile
ADD  updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE  nasdaqdb.companies_address 
ADD  updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE  nasdaqdb.companies_business_summary 
ADD  updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE  nasdaqdb.companies_fax 
ADD  updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE  nasdaqdb.companies_phone 
ADD  updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

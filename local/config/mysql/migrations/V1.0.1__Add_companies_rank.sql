CREATE TABLE nasdaqdb.companies_rank (
    id INT NOT NULL AUTO_INCREMENT,
    symbol VARCHAR(10) NOT NULL,
    name VARCHAR(200) NOT NULL,
    company_rank INT NOT NULL,
    company_id INT,
    FOREIGN KEY (company_id)
    REFERENCES companies(id),
    PRIMARY KEY (id));

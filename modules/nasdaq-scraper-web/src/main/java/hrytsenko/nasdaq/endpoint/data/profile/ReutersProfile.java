package hrytsenko.nasdaq.endpoint.data.profile;

public class ReutersProfile extends EndpointProfile {

	public ReutersProfile(String address, String businessSummary, String phone, String fax) {
		super(address, businessSummary, phone, fax);
	}

}

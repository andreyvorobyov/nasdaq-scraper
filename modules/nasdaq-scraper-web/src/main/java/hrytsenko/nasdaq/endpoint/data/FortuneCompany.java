package hrytsenko.nasdaq.endpoint.data;

import hrytsenko.nasdaq.domain.data.CompanyRank;

public class FortuneCompany {

	private final String symbol;
	private final String name;
	private final int rank;

	public FortuneCompany(String symbol, String name, int rank) {
		this.symbol = symbol;
		this.name = name;
		this.rank = rank;
	}

	public static CompanyRank toCompanyRank(FortuneCompany fortuneCompany) {
		CompanyRank companyRank = new CompanyRank();
		companyRank.setName(fortuneCompany.getName());
		companyRank.setSymbol(fortuneCompany.getSymbol());
		companyRank.setRank(fortuneCompany.getRank());
		return companyRank;
	}

	public String getSymbol() {
		return symbol;
	}

	public String getName() {
		return name;
	}

	public int getRank() {
		return rank;
	}
}

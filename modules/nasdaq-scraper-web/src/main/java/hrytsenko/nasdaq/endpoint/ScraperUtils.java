package hrytsenko.nasdaq.endpoint;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

public class ScraperUtils {
	private ScraperUtils() {
	}

	private static String getSubNodeText(int index, Optional<List<TextNode>> subNode) {
		if (index > subNode.get().size() - 1) {
			return "";
		} else {
			return subNode.get().get(index).text().trim();
		}
	}

	private static boolean containsPrefix(String text, String[] prefixs) {

		for (int i = 0; i < prefixs.length; i++) {
			if (text.toLowerCase().trim().contains(prefixs[i].toLowerCase().trim())) {
				return true;
			}
		}
		return false;
	}

	private static String cutePrefix(String text, String[] prefixs) {
		for (int i = 0; i < prefixs.length; i++) {
			text = text.toLowerCase().trim().replace(prefixs[i].toLowerCase().trim(), "");
		}
		return text;
	}

	public static Optional<List<TextNode>> extractSubNode(Elements elements) {
		if (elements.size() > 0) {
			return Optional.of(elements.get(0).textNodes());
		} else {
			return Optional.empty();
		}
	}

	public static String parseWhenInclude(Optional<List<TextNode>> subNode, String... prefixs) {
		if (!subNode.isPresent()) {
			return "";
		}
		return IntStream.range(0, subNode.get().size()).boxed()
				.filter(i -> containsPrefix(getSubNodeText(i, subNode), prefixs)).map(i -> getSubNodeText(i, subNode))
				.map(i -> cutePrefix(i, prefixs)).collect(Collectors.joining(" "));
	}

	public static String parseWhenNotInclude(Optional<List<TextNode>> subNode, String... prefixs) {
		if (!subNode.isPresent()) {
			return "";
		}
		return IntStream.range(0, subNode.get().size()).boxed()
				.filter(i -> !containsPrefix(getSubNodeText(i, subNode), prefixs)).map(i -> getSubNodeText(i, subNode))
				.collect(Collectors.joining(" "));
	}
}

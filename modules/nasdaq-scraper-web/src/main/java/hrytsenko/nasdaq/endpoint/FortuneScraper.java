package hrytsenko.nasdaq.endpoint;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.jboss.resteasy.spi.ApplicationException;
import org.jsoup.Jsoup;

import hrytsenko.nasdaq.domain.data.CompanyRank;
import hrytsenko.nasdaq.endpoint.data.FortuneCompany;

public class FortuneScraper {
	
	private FortuneScraper(){
		
	}

	public static List<CompanyRank> scrapRank(String link){
    	try{
    		
    		AtomicInteger rank = new AtomicInteger();
    		//timeout 10 min.
    		return Jsoup.parse(new URL(link),600000).select(".company-name-and-mkt-data").stream()
    				.map(e -> {
    					FortuneCompany fortuneCompany = new FortuneCompany(
    							e.select(".company-list-mkt-data").text().toUpperCase(),
    							e.select(".company-name").text(),
    							rank.incrementAndGet());
    					return FortuneCompany.toCompanyRank(fortuneCompany);
    				}).collect(Collectors.toList());
    	}catch (IOException exception) {
            throw new ApplicationException("Could not load rank from FORTUNE.", exception);
        }
    }
}

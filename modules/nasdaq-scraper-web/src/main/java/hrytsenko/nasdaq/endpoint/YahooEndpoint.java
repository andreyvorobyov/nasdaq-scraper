package hrytsenko.nasdaq.endpoint;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import hrytsenko.nasdaq.domain.data.Profile;
import hrytsenko.nasdaq.error.ApplicationException;
import hrytsenko.nasdaq.system.SettingsService;

@Stateless
public class YahooEndpoint {

	@Inject
    private SettingsService settingsService;

    public List<Profile> downloadCompaniesProfile(String symbol) {
        try {
        	String link = settingsService.getLinkForDownloadFromYahoo().replace("{symbol}", symbol.trim());
            return YahooScraper.scrapProfile(link);
        } catch (Exception exception) {
            throw new ApplicationException("Could not downoad data from YAHOO.", exception);
        }        
    }

}

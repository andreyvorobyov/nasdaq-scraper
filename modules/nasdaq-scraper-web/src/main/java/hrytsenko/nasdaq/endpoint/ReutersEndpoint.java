package hrytsenko.nasdaq.endpoint;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import hrytsenko.nasdaq.domain.data.Profile;
import hrytsenko.nasdaq.error.ApplicationException;
import hrytsenko.nasdaq.system.SettingsService;

@Stateless
public class ReutersEndpoint {
	@Inject
	private SettingsService settingsService;

	public List<Profile> downloadCompaniesProfile(String symbol) {
		try {
			String link = settingsService.getLinkForDownloadFromReuters().replace("{symbol}", symbol.trim());
			return ReutersScraper.scrapProfile(link);
		} catch (Exception exception) {
			throw new ApplicationException("Could not downoad data from REUTERS.", exception);
		}
	}
}

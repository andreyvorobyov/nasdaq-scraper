package hrytsenko.nasdaq.endpoint;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.TextNode;

import hrytsenko.nasdaq.domain.data.Profile;
import hrytsenko.nasdaq.endpoint.data.profile.ReutersProfile;

public class ReutersScraper {

	private ReutersScraper() {
	}

	public static List<Profile> scrapProfile(String link) throws Exception {
		Document doc = Jsoup.parse(new URL(link), 60000);

		return doc.select("#content .moduleBody .feature").stream().map(e -> {

			Optional<List<TextNode>> subNode = ScraperUtils
					.extractSubNode(doc.select("#content .moduleBody .feature > h2 + p"));

			String address = ScraperUtils.parseWhenNotInclude(subNode, "P:", "F:").trim();
			String businessSymmary = doc.select("#companyNews .moduleBody").text().trim();
			String phone = ScraperUtils.parseWhenInclude(subNode, "P:").trim();
			String fax = ScraperUtils.parseWhenInclude(subNode, "F:").trim();

			return new ReutersProfile(address, businessSymmary, phone, fax);
		}).map(ReutersProfile::toCompanyProfile).collect(Collectors.toList());
	}
}

package hrytsenko.nasdaq.endpoint;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jboss.resteasy.spi.ApplicationException;

import hrytsenko.nasdaq.domain.data.CompanyRank;
import hrytsenko.nasdaq.system.SettingsService;

@Stateless
public class FortuneEndpoint {

	@Inject
	SettingsService settingsService;
	
	public List<CompanyRank> downloadCompaniesRank(){
    	try {
            String link = settingsService.getLinkForDownloadFromFortune();
            return FortuneScraper.scrapRank(link);
        } catch (ApplicationException exception) {
            throw new ApplicationException("Could not downoad data from FORTUNE.", exception);
        }
    }
}

package hrytsenko.nasdaq.endpoint.data.profile;

public class YahooProfile extends EndpointProfile{

	public YahooProfile(String address, String businessSummary, String phone, String fax) {
		super(address, businessSummary, phone, fax);
	}
}

package hrytsenko.nasdaq.endpoint;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.TextNode;

import hrytsenko.nasdaq.domain.data.Profile;
import hrytsenko.nasdaq.endpoint.data.profile.YahooProfile;

public class YahooScraper {

	private YahooScraper() {
	}

	public static List<Profile> scrapProfile(String link) throws Exception {
		Document doc = Jsoup.parse(new URL(link), 60000);

		return doc.select("#yfncsumtab").stream().map(e -> {

			Optional<List<TextNode>> subNode = ScraperUtils.extractSubNode(doc.select(".yfnc_modtitlew1"));

			String address = ScraperUtils.parseWhenNotInclude(subNode, "Phone:", "Fax:", "Website:").trim();
			String businessSummary = e.select(".yfnc_modtitle1 + p").text().trim();
			String phone = ScraperUtils.parseWhenInclude(subNode, "Phone:").trim();
			String fax = ScraperUtils.parseWhenInclude(subNode, "Fax:").trim();

			return new YahooProfile(address, businessSummary, phone, fax);
		}).map(YahooProfile::toCompanyProfile).collect(Collectors.toList());

	}
}

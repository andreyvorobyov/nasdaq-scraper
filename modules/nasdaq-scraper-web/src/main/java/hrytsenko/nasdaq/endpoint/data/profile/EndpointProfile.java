package hrytsenko.nasdaq.endpoint.data.profile;

import com.google.common.base.Strings;

import hrytsenko.nasdaq.domain.data.Profile;
import hrytsenko.nasdaq.error.ApplicationException;

public abstract class EndpointProfile {
	private final String address;
	private final String businessSummary;
	private final String phone;
	private final String fax;

	public EndpointProfile(String address, String businessSummary, String phone, String fax) {
		this.address = address.length() > 200 ? address.substring(0, 200) : address;
		this.businessSummary = businessSummary;
		this.phone = phone.length() > 15 ? phone.substring(0, 15) : phone;
		this.fax = fax.length() > 15 ? fax.substring(0, 15) : fax;
	}

	public static Profile toCompanyProfile(EndpointProfile yahooProfile) {
		Profile profile = new Profile();
		fillProperty(yahooProfile, profile, "Address", "BusinessSummary", "Phone", "Fax");
		return profile;
	}

	public static void fillProperty(EndpointProfile fromProfile, Profile toProfile, String... properties) {
		try {
			for (String property : properties) {
				String value = (String) fromProfile.getClass().getMethod("get" + property).invoke(fromProfile);
				if (!Strings.isNullOrEmpty(value)) {
					toProfile.getClass().getMethod("add" + property, String.class).invoke(toProfile, value);
				}
			}
		} catch (Exception exception) {
			throw new ApplicationException("Could not fill property", exception);
		}
	}

	public String getAddress() {
		return address;
	}

	public String getBusinessSummary() {
		return businessSummary;
	}

	public String getPhone() {
		return phone;
	}

	public String getFax() {
		return fax;
	}
}

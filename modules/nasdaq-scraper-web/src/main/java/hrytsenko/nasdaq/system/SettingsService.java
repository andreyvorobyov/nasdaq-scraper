package hrytsenko.nasdaq.system;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.google.common.base.Splitter;
import com.google.common.collect.Maps;
import com.google.common.io.Resources;

import hrytsenko.nasdaq.error.ApplicationException;

@Startup
@Singleton
@Lock(LockType.READ)
public class SettingsService {

	private static final Logger LOGGER = Logger.getLogger(SettingsService.class.getName());

	private static final String SETTINGS_FILE = "app.properties";

	private Map<String, String> settings;

	private  Map<String, String> loadSettings(String filename) {
		
		LOGGER.info(() -> String.format("Load settings from file %s.", filename));

		try {
			Properties properties = new Properties();
			byte[] content = Resources.toByteArray(Resources.getResource(filename));
			properties.load(new ByteArrayInputStream(content));

			overrideSettingsByJndi(properties,"exchanges", "java:jboss/exchanges");
			overrideSettingsByJndi(properties,"link_for_download_from_fortune", "java:jboss/link_for_download_from_fortune");
			overrideSettingsByJndi(properties,"link_for_download_by_exchange", "java:jboss/link_for_download_by_exchange");
			overrideSettingsByJndi(properties,"link_for_download_from_yahoo", "java:jboss/link_for_download_from_yahoo");
			overrideSettingsByJndi(properties,"link_for_download_from_reuters", "java:jboss/link_for_download_from_reuters");

			return Maps.fromProperties(properties);
		} catch (IOException exception) {
			throw new ApplicationException("Could not read properties", exception);
		}
	}

	private static void overrideSettingsByJndi(Properties properties, String key, String jndiName) {

		Optional<String> settingValue ;
		try {
			settingValue = Optional.ofNullable((String) new InitialContext().lookup(jndiName));
		} catch (NamingException exeption) {
			settingValue = Optional.ofNullable(null);
		}		
		settingValue.ifPresent(v->properties.setProperty(key, v));		
	}
	
	@PostConstruct
	public void initSettings() {
		this.settings = Collections.unmodifiableMap(loadSettings(SETTINGS_FILE));
	}

	public String getLinkForDownloadByExchange() {
		return get("link_for_download_by_exchange");
	}

	public List<String> getExchanges() {
		return Splitter.on(',').trimResults().splitToList(get("exchanges"));
	}

	public String getLinkForDownloadFromFortune() {
		return get("link_for_download_from_fortune");
	}

	public String getLinkForDownloadFromYahoo(){
		return get("link_for_download_from_yahoo");
	}
	
	public String getLinkForDownloadFromReuters(){
		return get("link_for_download_from_reuters");
	}
	
	private String get(String name) {
		return settings.get(name);
	}

}

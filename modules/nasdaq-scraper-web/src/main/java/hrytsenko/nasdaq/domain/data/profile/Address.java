package hrytsenko.nasdaq.domain.data.profile;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.common.base.Objects;

import hrytsenko.nasdaq.domain.data.Profile;

@Entity
@Table(name = "companies_address")
public class Address {

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "address", length = 200)
	private String address;

	@ManyToOne(targetEntity = Profile.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "companyProfile_id")
	private Profile profile;

	public Address() {
	}

	public Address(String address, Profile profile) {
		this.address = address;
		this.profile = profile;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.address);
	}

	@Override
	public boolean equals(Object other) {
		return Objects.equal(((Address)other).address, this.address);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Profile getCompanyProfile() {
		return profile;
	}

	public void setCompanyProfile(Profile profile) {
		this.profile = profile;
	}
}

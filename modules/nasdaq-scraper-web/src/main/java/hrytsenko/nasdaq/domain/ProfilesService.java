package hrytsenko.nasdaq.domain;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import hrytsenko.nasdaq.domain.data.Company;
import hrytsenko.nasdaq.domain.data.Profile;

@Stateless
public class ProfilesService {
	
	@Inject
	ProfilesRepository profilesRepository;

	public List<Profile> findCompanyProfile(Company company, String dataSourse) {
		return profilesRepository.findCompaniesProfile(company, dataSourse);
	}

	public void updateCompanyProfile(Profile profile) {
		profilesRepository.updateCompanyProfile(profile);
	}
}

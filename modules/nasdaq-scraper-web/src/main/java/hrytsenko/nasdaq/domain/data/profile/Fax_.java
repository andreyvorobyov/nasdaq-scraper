package hrytsenko.nasdaq.domain.data.profile;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import hrytsenko.nasdaq.domain.data.Profile;

@StaticMetamodel(Fax.class)
public class Fax_ {

	public static volatile SingularAttribute<Fax, Long> id;
	public static volatile SingularAttribute<Fax, String> fax;
	public static volatile SingularAttribute<Fax,Profile> profile;

}

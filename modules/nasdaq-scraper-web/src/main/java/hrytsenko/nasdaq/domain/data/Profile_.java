package hrytsenko.nasdaq.domain.data;

import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import hrytsenko.nasdaq.domain.data.profile.Address;
import hrytsenko.nasdaq.domain.data.profile.BusinessSummary;
import hrytsenko.nasdaq.domain.data.profile.Fax;
import hrytsenko.nasdaq.domain.data.profile.Phone;

@StaticMetamodel(Profile.class)
public class Profile_ {

	public static volatile SingularAttribute<Profile, Long> id;
	public static volatile SingularAttribute<Profile, String> dataSourse;
	public static volatile SetAttribute<Profile, Address> address;
	public static volatile SetAttribute<Profile, BusinessSummary> businessSummary;
	public static volatile SetAttribute<Profile, Phone> phone;
	public static volatile SetAttribute<Profile, Fax> fax;
	public static volatile SingularAttribute<Profile, Company> company;
}

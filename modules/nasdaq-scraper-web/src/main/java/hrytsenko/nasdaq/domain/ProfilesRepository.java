package hrytsenko.nasdaq.domain;

import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import hrytsenko.nasdaq.domain.data.Company;
import hrytsenko.nasdaq.domain.data.Profile;
import hrytsenko.nasdaq.domain.data.Profile_;

@Stateless
public class ProfilesRepository {

	@PersistenceContext(unitName = "nasdaq-unit")
	private EntityManager entityManager;

	public List<Profile> findCompaniesProfile(Company company, String dataSourse) {
		return findCompaniesProfile(Optional.ofNullable(company), Optional.ofNullable(dataSourse));
	}

	public void updateCompanyProfile(Profile profile) {

		Profile updatedCompanyProfile = findCompaniesProfile(Optional.of(profile.getCompany()),
				Optional.of(profile.getDataSourse())).stream().findFirst().orElse(profile);

		updatedCompanyProfile.setCompany(profile.getCompany());
		Profile.copyProperty(profile, updatedCompanyProfile, "Address", "BusinessSummary", "Phone",
				"Fax");
		entityManager.persist(updatedCompanyProfile);
	}

	private List<Profile> findCompaniesProfile(Optional<Company> company, Optional<String> dataSourse) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Profile> query = builder.createQuery(Profile.class);
		Root<Profile> root = query.from(Profile.class);
		query.select(root);

		query.where(builder.and(
				company.map(value -> builder.equal(root.get(Profile_.company), value))
						.orElse(builder.conjunction()),
				dataSourse.map(value -> builder.equal(root.get(Profile_.dataSourse), value))
						.orElse(builder.conjunction())));

		return entityManager.createQuery(query).getResultList();
	}
}

package hrytsenko.nasdaq.domain.data;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(CompanyRank.class)
public class CompanyRank_ {
	public static volatile SingularAttribute<CompanyRank, Long> id;
	public static volatile SingularAttribute<CompanyRank, String> symbol;
	public static volatile SingularAttribute<CompanyRank, String> name;
	public static volatile SingularAttribute<CompanyRank, Integer> rank;
	public static volatile SingularAttribute<CompanyRank, Company> company;
}

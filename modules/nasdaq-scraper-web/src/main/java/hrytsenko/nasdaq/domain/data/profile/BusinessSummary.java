package hrytsenko.nasdaq.domain.data.profile;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.common.base.Objects;

import hrytsenko.nasdaq.domain.data.Profile;

@Entity
@Table(name = "companies_business_summary")

public class BusinessSummary {

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "business_summary")
	private String businessSummary;

	@ManyToOne(targetEntity = Profile.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "companyProfile_id", nullable = false)
	private Profile profile;

	public BusinessSummary() {

	}

	public BusinessSummary(String businessSummary, Profile profile) {
		this.businessSummary = businessSummary;
		this.profile = profile;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.businessSummary);
	}

	@Override
	public boolean equals(Object other) {
		return Objects.equal(((BusinessSummary)other).businessSummary, this.businessSummary);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBusinessSummary() {
		return businessSummary;
	}

	public void setBusinessSummary(String businessSummary) {
		this.businessSummary = businessSummary;
	}

	public Profile getCompanyProfile() {
		return profile;
	}

	public void setCompanyProfile(Profile profile) {
		this.profile = profile;
	}
}

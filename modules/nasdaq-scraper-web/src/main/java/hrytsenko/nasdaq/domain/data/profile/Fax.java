package hrytsenko.nasdaq.domain.data.profile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.common.base.Objects;

import hrytsenko.nasdaq.domain.data.Profile;

@Entity
@Table(name = "companies_fax")

public class Fax {
	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "fax", length = 15)
	private String fax;

	@ManyToOne(targetEntity = Profile.class)
	@JoinColumn(name = "companyProfile_id", nullable = false)
	private Profile profile;

	public Fax() {
	}

	public Fax(String fax, Profile profile) {
		this.fax = fax;
		this.profile = profile;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.fax);
	}

	@Override
	public boolean equals(Object other) {
		return Objects.equal(((Fax) other).fax, this.fax);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public Profile getCompanyProfile() {
		return profile;
	}

	public void setCompanyProfile(Profile profile) {
		this.profile = profile;
	}
}

package hrytsenko.nasdaq.domain;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import hrytsenko.nasdaq.domain.data.CompanyRank;

@Stateless
public class CompaniesRankService {

	@Inject
	CompaniesRankRepository companiesRankRepository;

	public List<CompanyRank> findCompanyRank(String symbol) {
		return companiesRankRepository.findCompaniesRank(symbol);
	}

	public void updateCompanyRank(CompanyRank companyRank) {
		companiesRankRepository.updateCompanyRank(companyRank);
	}
}

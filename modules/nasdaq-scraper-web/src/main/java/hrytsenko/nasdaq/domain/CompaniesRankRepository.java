package hrytsenko.nasdaq.domain;

import java.util.List;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import hrytsenko.nasdaq.domain.data.CompanyRank;
import hrytsenko.nasdaq.domain.data.CompanyRank_;

@Stateless
public class CompaniesRankRepository {

	@PersistenceContext(unitName = "nasdaq-unit")
    private EntityManager entityManager;
		
	@Inject
	CompaniesRepository companiesRepository;
	
	public List<CompanyRank> findCompaniesRank(String symbol) {
        return findCompaniesRank(Optional.ofNullable(symbol));
    }
	
    public void updateCompanyRank(CompanyRank companyRank) {
        CompanyRank updatedCompanyRank = findCompaniesRank(Optional.of(companyRank.getSymbol()))
        		.stream().findFirst().orElse(companyRank);

        updatedCompanyRank.setName(companyRank.getName());
        updatedCompanyRank.setSymbol(companyRank.getSymbol());
        updatedCompanyRank.setRank(companyRank.getRank());
        updatedCompanyRank.setCompany(
        		companiesRepository.findCompanies(null, companyRank.getSymbol(), null)
        		.stream().findFirst().orElse(null));
        
        entityManager.merge(updatedCompanyRank);
    }
    
    private List<CompanyRank> findCompaniesRank(Optional<String> symbol) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<CompanyRank> query = builder.createQuery(CompanyRank.class);
        Root<CompanyRank> root = query.from(CompanyRank.class);
        query.select(root);

        symbol.ifPresent(value -> query.where(builder.equal(root.get(CompanyRank_.symbol), value)));
       
        return entityManager.createQuery(query).getResultList();
    }
}


package hrytsenko.nasdaq.domain.data.profile;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import hrytsenko.nasdaq.domain.data.Profile;

@StaticMetamodel(Phone.class)
public class Phone_ {
	public static volatile SingularAttribute<Phone, Long> id;
	public static volatile SingularAttribute<Phone, String> phone;
	public static volatile SingularAttribute<Phone, Profile> profile;

}

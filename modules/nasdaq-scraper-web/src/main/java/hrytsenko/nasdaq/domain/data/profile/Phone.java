package hrytsenko.nasdaq.domain.data.profile;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.common.base.Objects;

import hrytsenko.nasdaq.domain.data.Profile;

@Entity
@Table(name = "companies_phone")

public class Phone {
	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "phone", length = 15)
	private String phone;

	@ManyToOne(targetEntity = Profile.class)
	@JoinColumn(referencedColumnName = "id", name = "companyProfile_id", nullable = false, insertable = true, updatable = true)
	private Profile profile;

	public Phone() {
	}

	public Phone(String phone, Profile profile) {
		this.phone = phone;
		this.profile = profile;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.phone);
	}

	@Override
	public boolean equals(Object other) {
		return Objects.equal(((Phone) other).phone, this.phone);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Profile getCompanyProfile() {
		return profile;
	}

	public void setCompanyProfile(Profile profile) {
		this.profile = profile;
	}
}

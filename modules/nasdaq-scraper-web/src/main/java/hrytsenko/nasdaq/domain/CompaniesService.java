package hrytsenko.nasdaq.domain;

import hrytsenko.nasdaq.domain.data.Company;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class CompaniesService {

    @Inject
    private CompaniesRepository companiesRepository;

    public List<Company> findCompanies(String exchange, String symbol, String sector) {
        return companiesRepository.findCompanies(exchange, symbol, sector);
    }

    public void updateCompany(Company company) {
        companiesRepository.updateCompany(company);
    }

    public List<String> findSectors() {
        return companiesRepository.findSectors();
    }

    public List<Company> findCompaniesForUpdate(int limit){
    	return companiesRepository.findCompaniesForUpdate(limit);
    }
    
    public void updateCounter(Company company){
    	companiesRepository.updateCounter(company);
    }
}

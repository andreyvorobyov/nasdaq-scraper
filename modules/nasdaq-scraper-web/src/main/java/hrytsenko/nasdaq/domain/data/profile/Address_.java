package hrytsenko.nasdaq.domain.data.profile;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import hrytsenko.nasdaq.domain.data.Profile;

@StaticMetamodel(Address.class)
public class Address_ {

	public static volatile SingularAttribute<Address, Long> id;
	public static volatile SingularAttribute<Address, String> address;
	public static volatile SingularAttribute<Address,Profile> profile;
	
}

package hrytsenko.nasdaq.domain.data;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import hrytsenko.nasdaq.domain.data.profile.Address;
import hrytsenko.nasdaq.domain.data.profile.BusinessSummary;
import hrytsenko.nasdaq.domain.data.profile.Fax;
import hrytsenko.nasdaq.domain.data.profile.Phone;
import hrytsenko.nasdaq.error.ApplicationException;

@Entity
@Table(name = "companies_profile")
public class Profile {
	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "data_sourse")
	private String dataSourse;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "profile", cascade = CascadeType.ALL, targetEntity = Address.class, orphanRemoval = true)
	private Set<Address> address;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "profile", cascade = CascadeType.ALL, targetEntity = BusinessSummary.class, orphanRemoval = true)
	private Set<BusinessSummary> businessSummary;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "profile", cascade = CascadeType.ALL, targetEntity = Fax.class, orphanRemoval = true)
	private Set<Fax> fax;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "profile", cascade = CascadeType.ALL, targetEntity = Phone.class, orphanRemoval = true)
	private Set<Phone> phone;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(referencedColumnName = "id", nullable = false)
	private Company company;

	public Profile() {
		address = new HashSet<>();
		businessSummary = new HashSet<>();
		fax = new HashSet<>();
		phone = new HashSet<>();
	}

	public static void copyProperty(Profile fromProfile, Profile toProfile, String... properties) {
		try {
			for (String property : properties) {
				Set<?> values = (Set<?>) fromProfile.getClass().getMethod("get" + property).invoke(fromProfile);
				Iterator<?> iterator = values.iterator();
				while (iterator.hasNext()) {
					Object obj = iterator.next();
					String value = (String) obj.getClass().getMethod("get" + property).invoke(obj);
					toProfile.getClass().getMethod("add" + property, String.class).invoke(toProfile, value);
				}
			}
		} catch (Exception exception) {
			throw new ApplicationException("Could not fill profile property", exception);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDataSourse() {
		return dataSourse;
	}

	public void setDataSourse(String dataSourse) {
		this.dataSourse = dataSourse;
	}

	public Set<Address> getAddress() {
		return address;
	}

	public void addAddress(String address) {
		this.address.add(new Address(address, this));
	}

	public Set<BusinessSummary> getBusinessSummary() {
		return businessSummary;
	}

	public void addBusinessSummary(String businessSummary) {
		this.businessSummary.add(new BusinessSummary(businessSummary, this));
	}

	public Set<Fax> getFax() {
		return fax;
	}

	public void addFax(String fax) {
		this.fax.add(new Fax(fax, this));
	}

	public Set<Phone> getPhone() {
		return phone;
	}

	public void addPhone(String phone) {
		this.phone.add(new Phone(phone, this));
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

}

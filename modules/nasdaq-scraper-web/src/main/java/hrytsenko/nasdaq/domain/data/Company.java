package hrytsenko.nasdaq.domain.data;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "companies")
public class Company {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "exchange")
    private String exchange;
    @Column(name = "symbol")
    private String symbol;
    @Column(name = "name")
    private String name;

    @Column(name = "sector")
    private String sector;
    @Column(name = "subsector")
    private String subsector;

    @OneToMany(targetEntity = CompanyRank.class, fetch =FetchType.LAZY)
    @JoinColumn(name = "company_id",updatable = false, nullable = true)
    private List<CompanyRank> ranks;
    
    @OneToMany(targetEntity = Profile.class, fetch =FetchType.LAZY)
    @JoinColumn(name = "company_id",updatable = false, nullable = true)
    private List<Profile> profiles;
    
    @OneToOne(mappedBy="company", targetEntity=UpdateCounter.class)
	private UpdateCounter updateCounter;
	
    public Company() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getSubsector() {
        return subsector;
    }

    public void setSubsector(String subsector) {
        this.subsector = subsector;
    }

	public List<CompanyRank> getRanks() {
		return ranks;
	}

	public void setRanks(List<CompanyRank> ranks) {
		this.ranks = ranks;
	}

	public List<Profile> getProfiles() {
		return profiles;
	}

	public void setProfiles(List<Profile> profiles) {
		this.profiles = profiles;
	}

	public UpdateCounter getUpdateCounter() {
		return updateCounter;
	}

	public void setUpdateCounter(UpdateCounter updateCounter) {
		this.updateCounter = updateCounter;
	}

}

package hrytsenko.nasdaq.domain.data;

import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Company.class)
public class Company_ {

    public static volatile SingularAttribute<Company, Long> id;

    public static volatile SingularAttribute<Company, String> exchange;
    public static volatile SingularAttribute<Company, String> symbol;
    public static volatile SingularAttribute<Company, String> name;
    public static volatile SingularAttribute<Company, String> sector;
    public static volatile SingularAttribute<Company, String> subsector;
    public static volatile ListAttribute<Company,CompanyRank> ranks;
    public static volatile ListAttribute<Company,Profile> profiles;
    public static volatile SingularAttribute<Company, UpdateCounter> updateCounter;
}

package hrytsenko.nasdaq.domain.data;

import java.util.Date;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(UpdateCounter.class)
public class UpdateCounter_ {

	public static volatile SingularAttribute<UpdateCounter, Long> id;
	public static volatile SingularAttribute<UpdateCounter, Company> company;
	public static volatile SingularAttribute<UpdateCounter, Date> lastUpdate;

}
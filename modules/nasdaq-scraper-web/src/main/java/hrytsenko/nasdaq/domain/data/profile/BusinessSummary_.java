package hrytsenko.nasdaq.domain.data.profile;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import hrytsenko.nasdaq.domain.data.Profile;


@StaticMetamodel(BusinessSummary.class)
public class BusinessSummary_ {

	public static volatile SingularAttribute<BusinessSummary, Long> id;
	public static volatile SingularAttribute<BusinessSummary, String> businessSummary;
	public static volatile SingularAttribute<BusinessSummary,Profile> profile;

}

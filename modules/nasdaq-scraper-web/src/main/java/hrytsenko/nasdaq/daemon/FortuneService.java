package hrytsenko.nasdaq.daemon;

import java.util.logging.Logger;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;

import hrytsenko.nasdaq.domain.CompaniesRankService;
import hrytsenko.nasdaq.endpoint.FortuneEndpoint;
import hrytsenko.nasdaq.system.stopwatch.WithStopwatch;

@Stateless
public class FortuneService {
	private static final Logger LOGGER = Logger.getLogger(NasdaqService.class.getName());

	@Inject
	private CompaniesRankService companiesRankService;
	@Inject
	private FortuneEndpoint fortuneEndpoint;

	@Asynchronous
	@WithStopwatch
	public void updateCompaniesRank() {
		LOGGER.info("Update companies rank.");
		fortuneEndpoint.downloadCompaniesRank().stream().unordered().parallel()
		.forEach(companiesRankService::updateCompanyRank);
	}

}

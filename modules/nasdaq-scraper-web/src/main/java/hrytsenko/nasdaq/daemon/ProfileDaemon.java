package hrytsenko.nasdaq.daemon;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;

@Startup
@Singleton
public class ProfileDaemon {
	@Resource
	private TimerService timerService;

	@Inject
	private ProfileService profileService;

	@PostConstruct
	public void initDaemon() {
		TimerConfig config = new TimerConfig(getClass().getSimpleName(), false);
		timerService.createSingleActionTimer(0, config);
		timerService.createCalendarTimer(new ScheduleExpression().hour("*").minute("*/5"), config);
	}

	@Timeout
	public void updateCompaniesProfile() {
		profileService.updateProfile();
	}
}

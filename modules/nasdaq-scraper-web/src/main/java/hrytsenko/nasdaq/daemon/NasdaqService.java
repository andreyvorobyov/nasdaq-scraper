package hrytsenko.nasdaq.daemon;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;

import hrytsenko.nasdaq.domain.CompaniesService;
import hrytsenko.nasdaq.endpoint.NasdaqEndpoint;
import hrytsenko.nasdaq.system.stopwatch.WithStopwatch;

@Stateless
public class NasdaqService {

    private static final Logger LOGGER = Logger.getLogger(NasdaqService.class.getName());

    @Inject
    private CompaniesService companiesService;
    @Inject
    private NasdaqEndpoint nasdaqEndpoint;

    @Asynchronous
    @WithStopwatch
    public void updateCompanies(List<String> exchanges) {
        exchanges.stream().unordered().parallel()
        .peek(exchange -> LOGGER.info(() -> String.format("Update companies for %s.", exchange)))
                .forEach(exchange -> nasdaqEndpoint.downloadCompanies(exchange)
                		.stream().unordered().parallel()
                        .forEach(companiesService::updateCompany));
    }

}
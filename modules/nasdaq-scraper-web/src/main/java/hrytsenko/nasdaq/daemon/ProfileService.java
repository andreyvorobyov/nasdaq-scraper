package hrytsenko.nasdaq.daemon;

import java.util.logging.Logger;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;

import hrytsenko.nasdaq.domain.CompaniesService;
import hrytsenko.nasdaq.domain.ProfilesService;
import hrytsenko.nasdaq.endpoint.ReutersEndpoint;
import hrytsenko.nasdaq.endpoint.YahooEndpoint;
import hrytsenko.nasdaq.system.stopwatch.WithStopwatch;

@Stateless
public class ProfileService {

	private static final Logger LOGGER = Logger.getLogger(ProfileService.class.getName());

	@Inject
	private ProfilesService profilesService;
	@Inject
	private YahooEndpoint yahooEndpoint;
	@Inject
	private ReutersEndpoint reutersEndpoint;
	@Inject
	private CompaniesService companiesService;

	@Asynchronous
	@WithStopwatch
	public void updateProfile() {
		
		LOGGER.info("Update companies profile.");
		
		companiesService.findCompaniesForUpdate(100)
		.stream().forEach(company->{			
			
			yahooEndpoint.downloadCompaniesProfile(company.getSymbol())
			.stream().forEach(profile->{
				profile.setCompany(company);
				profile.setDataSourse("YAHOO");			
				profilesService.updateCompanyProfile(profile);				
			});
			
			reutersEndpoint.downloadCompaniesProfile(company.getSymbol())
			.stream().forEach(profile->{
				profile.setCompany(company);
				profile.setDataSourse("REUTERS");
				profilesService.updateCompanyProfile(profile);
			});
			
			companiesService.updateCounter(company);
		});
		
	}
}

package hrytsenko.nasdaq.client;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.google.common.base.Strings;

import hrytsenko.nasdaq.client.data.CompanySoapClient;
import hrytsenko.nasdaq.domain.CompaniesService;

@WebService(name="CompaniesSoapService", targetNamespace="http://www.nasdaq-scraper.org/ws/service")
public class CompaniesSoapService {

	@Inject
	private CompaniesService companiesService;
	
	@WebMethod(action="findCompanies")
	@WebResult(name="company")
	public List<CompanySoapClient> findCompanies(@WebParam(name="exhange") String exchange,
			@WebParam(name="symbol") String symbol, @WebParam(name="sector") String sector){
		return companiesService
                .findCompanies(Strings.emptyToNull(exchange), Strings.emptyToNull(symbol), Strings.emptyToNull(sector))
                .stream().map(CompanySoapClient::fromCompany).collect(Collectors.toList());
	}
	
	@WebMethod(action="findSectors")
	@WebResult(name="sector")
	public List<String> findSectors(){
		return companiesService.findSectors();
	}
}

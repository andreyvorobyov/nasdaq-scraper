package hrytsenko.nasdaq.client.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import hrytsenko.nasdaq.domain.data.Company;

@XmlRootElement(name="client",namespace="http://www.nasdaq-scraper.org/ws/service")
@XmlAccessorType(XmlAccessType.NONE)
public class CompanySoapClient {

	@XmlElement(name="exchange")
    private  String exchange;
	
	@XmlElement(name="symbol")
    private  String symbol;
	
	@XmlElement(name="name")
    private  String name;
	
	@XmlElement(name="sector")
    private  String sector;
	
	@XmlElement(name="subsector")
    private  String subsector;

	public CompanySoapClient(){
		
	}
	
    public CompanySoapClient(String exchange, String symbol, String name, String sector, String subsector) {
        this.exchange = exchange;
        this.symbol = symbol;
        this.name = name;
        this.sector = sector;
        this.subsector = subsector;
    }

    public static CompanySoapClient fromCompany(Company company) {
        return new CompanySoapClient(company.getExchange(), company.getSymbol(), company.getName(), company.getSector(),
                company.getSubsector());
    }

    public String getExchange() {
        return exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getName() {
        return name;
    }

    public String getSector() {
        return sector;
    }

    public String getSubsector() {
        return subsector;
    }

}

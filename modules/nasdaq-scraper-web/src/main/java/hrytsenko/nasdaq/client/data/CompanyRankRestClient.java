package hrytsenko.nasdaq.client.data;

import hrytsenko.nasdaq.domain.data.CompanyRank;

public class CompanyRankRestClient {
	private String symbol;
	private String name;
	private int rank;

	public CompanyRankRestClient() {

	}

	public CompanyRankRestClient(String symbol, String name, int rank) {
		this.symbol = symbol;
		this.name = name;
		this.rank = rank;
	}

	public static CompanyRankRestClient fromCompanyRank(CompanyRank companyRank) {
		return new CompanyRankRestClient(companyRank.getSymbol(), companyRank.getName(), companyRank.getRank());
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

}

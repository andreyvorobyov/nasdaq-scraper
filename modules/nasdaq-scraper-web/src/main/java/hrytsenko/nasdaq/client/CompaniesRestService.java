package hrytsenko.nasdaq.client;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.common.base.Strings;

import hrytsenko.nasdaq.client.data.CompanyRankRestClient;
import hrytsenko.nasdaq.client.data.CompanyRestClient;
import hrytsenko.nasdaq.client.data.ProfileClient;
import hrytsenko.nasdaq.domain.CompaniesRankService;
import hrytsenko.nasdaq.domain.CompaniesService;
import hrytsenko.nasdaq.domain.data.Company;
import hrytsenko.nasdaq.error.ApplicationException;

@Path("companies")
@Stateless
public class CompaniesRestService {

	@Inject
	private CompaniesService companiesService;

	@Inject
	private CompaniesRankService companiesRankService;

	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CompanyRestClient> findCompanies(@QueryParam("exchange") String exchange,
			@QueryParam("symbol") String symbol, @QueryParam("sector") String sector) {
		return getCompanies(exchange, symbol, sector);
	}

	@GET
	@Path("/sectors")
	@Produces(MediaType.APPLICATION_JSON)
	public List<String> findSectors() {
		return companiesService.findSectors();
	}

	@GET
	@Path("/ranks")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CompanyRankRestClient> findRanks(@QueryParam("symbol") String symbol) {
		return companiesRankService.findCompanyRank(symbol).stream().map(CompanyRankRestClient::fromCompanyRank)
				.collect(Collectors.toList());

	}

	@GET
	@Path("/xls")
	@Produces("application/vnd.ms-excel")
	public Response getExcelFile(@QueryParam("exchange") String exchange, @QueryParam("symbol") String symbol,
			@QueryParam("sector") String sector) {
		List<CompanyRestClient> companies = getCompanies(exchange, symbol, sector);
		return responseWithAttachment(Converter.toXlsx(companies), "Компании.xls");
	}

	@GET
	@Path("/csv")
	@Produces("text/csv")
	public Response getCSVFile(@QueryParam("exchange") String exchange, @QueryParam("symbol") String symbol,
			@QueryParam("sector") String sector) {
		List<CompanyRestClient> companies = getCompanies(exchange, symbol, sector);
		return responseWithAttachment(Converter.toCsv(companies), "Companies.csv");
	}

	@GET
	@Path("/pdf/{symbol}")
	@Produces("application/pdf")
	public Response getPdfFile(@PathParam("symbol") String symbol) {
		Company company = companiesService.findCompanies(null, symbol, null).stream().findFirst().orElse(null);
		if(company == null){
			return Response.status(Status.NOT_FOUND).build();
		}
		return responseWithContent(Converter.toPdf(ProfileClient.fromCompany(company)), "application/pdf");
	}

	private Response responseWithContent(byte[] file, String contentType) {
		return Response.ok(file).header("Content-Type: application/pdf", file).build();
	}

	private Response responseWithAttachment(byte[] file, String filename) {
		try {
			filename = URLEncoder.encode(filename, "UTF-8");
		} catch (UnsupportedEncodingException exception) {
			throw new ApplicationException("Could not encode filename.", exception);
		}
		return Response.ok(file).header("Content-Disposition", "attachment; filename=\"" + filename + "\"").build();
	}

	private List<CompanyRestClient> getCompanies(String exchange, String symbol, String sector) {
		return companiesService
				.findCompanies(Strings.emptyToNull(exchange), Strings.emptyToNull(symbol), Strings.emptyToNull(sector))
				.stream().map(CompanyRestClient::fromCompany).collect(Collectors.toList());
	}
}

package hrytsenko.nasdaq.client.data;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import hrytsenko.nasdaq.domain.data.Company;
import hrytsenko.nasdaq.domain.data.CompanyRank;

public class CompanyRestClient {

    private  String exchange;
    private  String symbol;
    private  String name;

    private  String sector;
    private  String subsector;

    private List<CompanyRankRestClient> ranks;
    
    public CompanyRestClient(){
    	
    }
    
    public CompanyRestClient(String exchange, String symbol, String name, String sector, String subsector,List<CompanyRank> ranks) {
        this.exchange = exchange;
        this.symbol = symbol;
        this.name = name;
        this.sector = sector;
        this.subsector = subsector;
        //if ranks is null return empty array list
        this.ranks = Optional.of(ranks).orElse(Arrays.asList()).stream()
        		.map(CompanyRankRestClient::fromCompanyRank)
        		.collect(Collectors.toList());
    }

    public static CompanyRestClient fromCompany(Company company) {
        return new CompanyRestClient(company.getExchange(), company.getSymbol(), company.getName(), company.getSector(),
                company.getSubsector(),company.getRanks());
    }

    public String getExchange() {
        return exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getName() {
        return name;
    }

    public String getSector() {
        return sector;
    }

    public String getSubsector() {
        return subsector;
    }
    
    public List<CompanyRankRestClient> getRanks(){
    	return ranks;
    }

}

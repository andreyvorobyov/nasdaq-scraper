package hrytsenko.nasdaq.client.data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import hrytsenko.nasdaq.domain.data.Company;

public class ProfileClient {

	private String exchange;
	private String symbol;
	private String name;
	private String sector;
	private String subsector;

	private List<String> address;
	private List<String> phones;
	private List<String> faxs;
	private List<String> businessSymarys;

	public ProfileClient() {

	}

	public ProfileClient(String exchange, String symbol, String name, String sector, String subsector) {
		this.exchange = exchange;
		this.symbol = symbol;
		this.name = name;
		this.sector = sector;
		this.subsector = subsector;
		this.address = new ArrayList<>();
		this.phones = new ArrayList<>();
		this.faxs = new ArrayList<>();
		this.businessSymarys = new ArrayList<>();
	}

	public static ProfileClient fromCompany(Company company) {
		ProfileClient profileClient = new ProfileClient(company.getExchange(), company.getSymbol(), company.getName(),
				company.getSector(), company.getSubsector());

		company.getProfiles().stream().forEach(profile -> {

			profileClient
					.setAddress(profile.getAddress().stream().map(i -> i.getAddress()).collect(Collectors.toList()));
			profileClient.setPhones(profile.getPhone().stream().map(i -> i.getPhone()).collect(Collectors.toList()));
			profileClient.setFaxs(profile.getFax().stream().map(i -> i.getFax()).collect(Collectors.toList()));
			profileClient.setBusinessSymarys(profile.getBusinessSummary().stream().map(i -> i.getBusinessSummary())
					.collect(Collectors.toList()));

		});

		return profileClient;
	}

	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getSubsector() {
		return subsector;
	}

	public void setSubsector(String subsector) {
		this.subsector = subsector;
	}

	public List<String> getAddress() {
		return address;
	}

	public void setAddress(List<String> address) {
		address.stream().forEach(this.address::add);
	}

	public List<String> getPhones() {
		return phones;
	}

	public void setPhones(List<String> phones) {
		 phones.stream().forEach(this.phones::add);
	}

	public List<String> getFaxs() {
		return faxs;
	}

	public void setFaxs(List<String> faxs) {
		faxs.stream().forEach(this.faxs::add);
	}

	public List<String> getBusinessSymarys() {
		return businessSymarys;
	}

	public void setBusinessSymarys(List<String> businessSymarys) {
		businessSymarys.parallelStream().forEach(this.businessSymarys::add);
	}

}

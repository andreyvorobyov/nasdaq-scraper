package hrytsenko.nasdaq.client;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import hrytsenko.nasdaq.client.data.CompanyRestClient;
import hrytsenko.nasdaq.client.data.ProfileClient;
import hrytsenko.nasdaq.error.ApplicationException;

public final class Converter {

	private static final Object[] FILE_HEADER = { "exchange", "symbol", "name", "sector", "subsector" };

	private static Font captionFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
	private static Font infoFont = new Font(Font.FontFamily.COURIER, 10, Font.ITALIC, BaseColor.BLUE);

	private Converter() {
	}

	public static byte[] toXlsx(List<CompanyRestClient> companies) {
		try (Workbook book = new HSSFWorkbook();) {
			Sheet sheet = book.createSheet("Companies");
			copyToRow(sheet.createRow(0), FILE_HEADER);
			for (int i = 0; i < companies.size(); i++) {
				CompanyRestClient company = companies.get(i);
				copyToRow(sheet.createRow(i + 1), company.getExchange(), company.getSymbol(), company.getName(),
						company.getSector(), company.getSubsector());
			}
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			book.write(baos);
			return baos.toByteArray();
		} catch (IOException exception) {
			throw new ApplicationException("file not created", exception);
		}
	}

	private static void copyToRow(Row row, Object... values) {
		for (int i = 0; i < values.length; i++) {
			row.createCell(i).setCellValue(String.valueOf(values[i]));
		}
	}

	public static byte[] toCsv(List<CompanyRestClient> companies) {
		StringBuilder result = new StringBuilder();
		CSVFormat csvFormat = CSVFormat.DEFAULT.withRecordSeparator("\n");
		try (CSVPrinter csvPrinter = new CSVPrinter(result, csvFormat)) {
			csvPrinter.printRecord(FILE_HEADER);
			for (CompanyRestClient company : companies) {
				csvPrinter.printRecord(Arrays.asList(company.getExchange(), company.getSymbol(), company.getName(),
						company.getSector(), company.getSubsector()));
			}
			return result.toString().getBytes();
		} catch (IOException exception) {
			throw new ApplicationException("file not created", exception);
		}
	}

	public static byte[] toPdf(ProfileClient profile) {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		Document document = new Document();
		try {
			PdfWriter.getInstance(document, baos);
			document.open();
			Paragraph paragraph = new Paragraph();

			addParagraph(paragraph, "EXCHANGE: ", profile.getExchange());
			addParagraph(paragraph, "SYMBOL: ", profile.getSymbol());
			addParagraph(paragraph, "NAME: ", profile.getName());
			addParagraph(paragraph, "SECTOR: ", profile.getSector());
			addParagraph(paragraph, "SUB SECTOR: ", profile.getSubsector());
			
			profile.getAddress().stream().forEach(i -> addParagraph(paragraph, "ADDRESS: ", i));
			profile.getPhones().stream().forEach(i -> addParagraph(paragraph, "PHONE: ", i));
			profile.getFaxs().stream().forEach(i -> addParagraph(paragraph, "FAX: ", i));
			profile.getBusinessSymarys().stream().forEach(i -> addParagraph(paragraph, "BUSINESS SYMMARY: ", i));

			document.add(paragraph);

		} catch (DocumentException exception) {
			throw new ApplicationException("file not created", exception);
		} finally {
			document.close();
		}
		return baos.toByteArray();
	}

	private static void addParagraph(Paragraph owner, String caption, String info) {
		Paragraph paragraph = new Paragraph(caption, captionFont);
		paragraph.add(new Paragraph(info, infoFont));
		owner.add(paragraph);
	}

}

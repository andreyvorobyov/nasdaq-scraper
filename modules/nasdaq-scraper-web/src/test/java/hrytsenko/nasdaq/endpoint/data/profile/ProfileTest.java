package hrytsenko.nasdaq.endpoint.data.profile;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import hrytsenko.nasdaq.domain.data.Profile;

public class ProfileTest {

	private YahooProfile yahooProfile;

	@Before
	public void init() {
		yahooProfile = new YahooProfile("Suite 300 Dallas, TX 75234 United States", "summary", "972-407-8400",
				"");
	}

	@Test
	public void fillPropertyTest() {
		Profile profile = new Profile();
		
		profile.addAddress("Suite 300 Dallas, TX 75234 United States");
		profile.addAddress("Suite 300 Dallas, TX 75234 United States");
		Assert.assertEquals (profile.getAddress().size(),1);
		profile.addAddress("Suite 300 Dallas");
		Assert.assertEquals (profile.getAddress().size(),2);
	
        YahooProfile.fillProperty(yahooProfile, profile, "Address","BusinessSummary","Phone","Fax");
		Assert.assertEquals(profile.getPhone().iterator().next().getPhone(), "972-407-8400");
		Assert.assertEquals(profile.getBusinessSummary().iterator().next().getBusinessSummary(), "summary");
		Assert.assertTrue(profile.getFax().size() == 0);
	}
}

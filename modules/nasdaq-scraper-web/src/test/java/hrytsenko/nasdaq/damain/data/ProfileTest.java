package hrytsenko.nasdaq.damain.data;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import hrytsenko.nasdaq.domain.data.Profile;

public class ProfileTest {

	private Profile fromProfile;
	private Profile toProfile;

	@Before
	public void init() {
		toProfile = new Profile();

		fromProfile = new Profile();
		fromProfile.addAddress("Suite 300 Dallas, TX 75234 United States");
	}

	@Test
	public void copePropertyTest() {
		Profile.copyProperty(fromProfile, toProfile, "Address");
		Assert.assertArrayEquals(fromProfile.getAddress().toArray(), toProfile.getAddress().toArray());
	}
}
